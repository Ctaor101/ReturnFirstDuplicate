﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Answer
    {
        static void Main(string[] args)
        {
            string test1 = "abca";
            string test2 = "bcaba";
            string test3 = "abc";
            string test4 = "dBcABA";

            //PrintResult(ReturnFirstDuplicateCharaters(test1));
            //PrintResult(ReturnFirstDuplicateCharaters(test2));
            //PrintResult(ReturnFirstDuplicateCharaters(test3));
            //PrintResult(ReturnFirstDuplicateCharaters(test4));

            PrintResult(DictionarySolution(test1));
            PrintResult(ListSolution(test1));

            PrintResult(DictionarySolution(test2));
            PrintResult(ListSolution(test2));

            PrintResult(DictionarySolution(test3));
            PrintResult(ListSolution(test3));

            PrintResult(DictionarySolution(test4));
            PrintResult(ListSolution(test4));

            Console.Read();
        }

       //This method will not work correctly.
       public static char ReturnFirstDuplicateCharaters(string testString)
        {
            var test = testString.ToLower();
            Console.WriteLine(test);
            //Used as a placeholder 

            char result = ' ';

            for (var i = 0; i < test.Length; i++)
            {
                //Handles the initial case and cases where it is reset.                 
                if (result == ' ')
                {
                    result = test[i];
                    continue;
                }

                //Handles if results match
                if (test[i] == result)
                {
                    return result;
                }                         
            }

            //Handles case if no other instance is found for any character
            result = ' ';

            return result;
        }
        
       public static char DictionarySolution (string testString)
        {
            var test = testString.ToLower();
            Console.WriteLine(test);

            Dictionary<char, int> result = new Dictionary<char, int>();

            for (var i = 0; i < test.Length; i++)
            {
                //Handles the initial case and cases where it is reset.                 
                if (result.Count == 0)
                {
                    result.Add(test[i], 1);
                    continue;
                }

                //Handles if results match
                if (result.ContainsKey(test[i]))
                {
                    return test[i];
                }

                //Add Checked Items to the Dictionary
                result.Add(test[i], 1);
            }          

            return ' ';
        }

       public static char ListSolution (string testString)
        {

            var test = testString.ToLower();
            Console.WriteLine(test);

            List<char> result = new List<char>();

            for (var i = 0; i < test.Length; i++)
            {
                //Handles the initial case and cases where it is reset.                 
                if (result.Count == 0)
                {
                    result.Add(test[i]);
                    continue;
                }

                //Handles if results match
                if (result.Contains(test[i]))
                {
                    return test[i];
                }

                //Add Checked Items to the List Example. 
                result.Add(test[i]);
            }

            return ' ';
        }

       public static void PrintResult(char result)
       {
            Console.WriteLine("The 1st duplicate in the string was: " + result);
            Console.WriteLine("");
       }
   }
}
