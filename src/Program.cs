﻿using System;
using System.Collections.Generic;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            string test1 = "abca";
            string test2 = "bcaba";
            string test3 = "abc";
            string test4 = "dBcABA"; // This Case will Fail, Current Method only handles when the 1st character is the duplicate, should use a dictionary. 

            //PrintResult(ReturnFirstDuplicateCharaters(test1));
            //PrintResult(ReturnFirstDuplicateCharaters(test2));
            //PrintResult(ReturnFirstDuplicateCharaters(test3));
            PrintResult(DictionarySolution(test4));
            PrintResult(ListSolution(test4));

            Console.Read();
        }


       public static char ReturnFirstDuplicateCharaters(string testString)
        {
            var test = testString.ToLower();
            Console.WriteLine(test);
            //Used as a placeholder 

            char result = ' ';

            for (var i = 0; i < test.Length; i++)
            {
                //Handles the initial case and cases where it is reset.                 
                if (result == ' ')
                {
                    result = test[i];
                    continue;
                }

                //Handles if results match
                if (test[i] == result)
                {
                    return result;
                }                         
            }

            //Handles case if no other instance is found for any character
            result = ' ';

            return result;
        }
        
        public static char DictionarySolution (string testString)
        {
            var test = testString.ToLower();

            Dictionary<char, int> result = new Dictionary<char, int>();

            for (var i = 0; i < test.Length; i++)
            {
                //Handles the initial case and cases where it is reset.                 
                if (result.Count == 0)
                {
                    result.Add(test[i], 1);
                    continue;
                }

                //Handles if results match
                if (result.ContainsKey(test[i]))
                {
                    return test[i];
                }

                //Add Checked Items to the Dictionary
                result.Add(test[i], 1);
            }          

            return ' ';
        }

        public static char ListSolution (string testString)
        {

            var test = testString.ToLower();

            List<char> result = new List<char>();

            for (var i = 0; i < test.Length; i++)
            {
                //Handles the initial case and cases where it is reset.                 
                if (result.Count == 0)
                {
                    result.Add(test[i]);
                    continue;
                }

                //Handles if results match
                if (result.Contains(test[i]))
                {
                    return test[i];
                }

                //Add Checked Items to the List Example. 
                result.Add(test[i]);
            }

            return ' ';
        }

       public static void PrintResult(char result)
       {
            Console.WriteLine("The 1st duplicate in the string was: " + result);
       }
   }
}
